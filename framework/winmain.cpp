
#include <Windows.h>
#include <tchar.h>

// 作ったヘッダーファイルを全部インクルード
#include "direct3d.h"
#include "texture.h"
#include "sprite.h"
#include "directsound.h"
#include "wave.h"
#include "soundbuffer.h"
#include "GraphicData.h"
#include <vector>
#include <deque>

// ウィンドウプロシージャ、ウィンドウに対するメッセージ処理を行う
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam){
	
	switch(msg){
		// ウィンドウが破棄されたとき
	case WM_DESTROY:
		PostQuitMessage(0);	// WM_QUITメッセージをメッセージキューに送る
		return 0;
	}
	// デフォルトのメッセージ処理を行う
	return DefWindowProc(hWnd, msg, wParam, lParam);
}


// WinMain関数（アプリケーションの開始関数）
// コンソールアプリケーションと違い、コンソールを開かない
int _stdcall WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	const TCHAR* WC_BASIC = _T("BASIC_WINDOW");
	// シンプルウィンドウクラス設定
	WNDCLASSEX wcex ={sizeof(WNDCLASSEX), CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,WndProc, 0,0,hInstance,
		(HICON)LoadImage(NULL,MAKEINTRESOURCE(IDI_APPLICATION),IMAGE_ICON,0,0,LR_DEFAULTSIZE | LR_SHARED),
		(HCURSOR)LoadImage(NULL,MAKEINTRESOURCE(IDC_ARROW),IMAGE_CURSOR,0,0,LR_DEFAULTSIZE | LR_SHARED), 
		(HBRUSH)GetStockObject(WHITE_BRUSH), NULL, WC_BASIC , NULL};

	// シンプルウィンドウクラス作成
	if(!RegisterClassEx(&wcex)) 
		return false;

	// ウィンドウ幅、高さはディスプレイに依存する。普通は4:3
	const int WINDOW_WIDTH = 640;
	const int WINDOW_HEIGHT = 480;
	// ウィンドウの作成
	HWND hWnd = CreateWindowEx(0,WC_BASIC,
		_T("Application"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE,
		CW_USEDEFAULT,CW_USEDEFAULT,WINDOW_WIDTH,WINDOW_HEIGHT,NULL,NULL,hInstance,NULL);

///////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////
	//	Direct3Dの初期化
	////////////////////////////////////
	direct3d.Create(hWnd,WINDOW_WIDTH,WINDOW_HEIGHT);

	int graphic0 = LoadGraph(_T("car0.bmp"));
	int graphic1 = LoadGraph(_T("car1.bmp"));
	int graphic2 = LoadGraph(_T("car2.bmp"));
	int graphic3 = LoadGraph(_T("car3.bmp"));
	int graphic4 = LoadGraph(_T("car4.bmp"));
	int graphic5 = LoadGraph(_T("car5.bmp"));
	int graphic6 = LoadGraph(_T("car0.bmp"));

	DeleteGraph(graphic4);
	
///////////////////////////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////
	// DirectSoundデバイス作成
	////////////////////////////////
	DirectSound directsound;
	directsound.Create(hWnd);

	///////////////////////////////////////////////
	//	Waveファイル読込み
	///////////////////////////////////////////////
	Wave wave[2];
	wave[0].Load(_T("katana.wav"));
	wave[1].Load(_T("bomb.wav"));
	

	///////////////////ファイル読込みはここまで

	/////////////////////////////////
	//	セカンダリバッファの作成
	//	(読み込んだ音データをコピー)
	/////////////////////////////////
	SoundBuffer sb[2];
	for(int i = 0;i < 2;++i)
		sb[i].Create(directsound.pDirectSound8,wave[i].WaveFormat,wave[i].WaveData,wave[i].DataSize);


	// ループ再生
	sb[0].Play(true);

///////////////////////////////////////////////////////////////////////////////////////////////

	// メッセージループ
	MSG msg = {}; 
	while(msg.message != WM_QUIT) {
		// アプリケーションに送られてくるメッセージをメッセージキューから取得する
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
			DispatchMessage(&msg);	// アプリケーションの各ウィンドウプロシージャにメッセージを転送する
		}
		// メッセージ処理をしていないとき
		else{
			//（ここにDirectXの処理を書く）

///////////////////////////////////////////////////////////////////////////////////////////////

			// 描画開始
			if(SUCCEEDED(direct3d.pDevice3D->BeginScene()))
			{
				DWORD ClearColor = 0xff808080;	// 背景クリア色
				// 背景クリア
				direct3d.pDevice3D->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_STENCIL | D3DCLEAR_ZBUFFER, ClearColor, 1.0f, 0 );

				// 再生
				if(GetAsyncKeyState('A'))
					sb[1].Play(false);
				
				DrawGraph(32, 32, graphic0);
				DrawGraph(96, 32, graphic1);
				DrawGraph(160, 32, graphic2);
				DrawGraph(224, 32, graphic3);
				//DrawGraph(288, 32, graphic4);
				DrawGraph(352, 32, graphic5);
				DrawGraph(416, 32, graphic6);
				
				// 描画終了
				direct3d.pDevice3D->EndScene();
			}
			// 描画反映
			direct3d.pDevice3D->Present( NULL, NULL, NULL, NULL );

///////////////////////////////////////////////////////////////////////////////////////////////
		}
	}

	AllDeleteGraph();
	
	return 0;
}
