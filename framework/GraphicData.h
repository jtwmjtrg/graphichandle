#pragma once
// 作ったヘッダーファイルを全部インクルード
#include "direct3d.h"
#include "texture.h"
#include "sprite.h"
#include "directsound.h"
#include "wave.h"
#include "soundbuffer.h"

#include <vector>
#include <deque>

using std::vector;
using std::deque;


// 【define】
#define LoadGraph GraphicData::LoadGraph
#define direct3d GraphicData::direct3d
#define DeleteGraph GraphicData::DeleteGraph
#define DrawGraph GraphicData::DrawGraph
#define AllDeleteGraph GraphicData::AllDeleteGraph


/////////////////////////
/* 仮置きグローバル先輩
static Direct3D direct3d;
vector<Texture> tex;
vector<Sprite> sprite;
vector<int> graphicmanager;
*////////////////////////

class GraphicData {
private:
	static vector<Texture> tex;
	static vector<Sprite> sprite;
	static vector<int> graphicmanager;
public:
	static Direct3D direct3d;

	GraphicData();
	~GraphicData();

	// 画像を読み込む関数
	static int LoadGraph(TCHAR* filename);

	// 読み込んだ画像データを削除する関数
	static void DeleteGraph(int handle);

	// 読み込んだ画像データを全て削除する関数
	static void AllDeleteGraph();

	// 画像を描写する関数
	static void DrawGraph(int x, int y, int handle);
};