#include "GraphicData.h"

GraphicData::GraphicData(){}

GraphicData::~GraphicData() {}

// static変数
Direct3D GraphicData::direct3d;
vector<Texture> GraphicData::tex;
vector<Sprite> GraphicData::sprite;
vector<int> GraphicData::graphicmanager;


// 画像を読み込む関数
int GraphicData::LoadGraph(TCHAR* filename)
{
	tex.emplace_back();
	tex.back().Load(direct3d.pDevice3D, filename);
	graphicmanager.push_back(tex.size() - 1);
	D3DXIMAGE_INFO info;
	D3DXGetImageInfoFromFile(filename, &info);
	sprite.emplace_back();
	sprite.back().SetWidth(info.Width, info.Height);

	return (graphicmanager.size() - 1);
}

// 読み込んだ画像データを削除する関数
void GraphicData::DeleteGraph(int handle)
{
	tex[graphicmanager[handle]].Delete();	// テクスチャ削除

	for (int i = (handle + 1), n = graphicmanager.size(); i < n; i++)
	{
		graphicmanager[i]--;
	}
	tex.erase(tex.begin() + handle);
	sprite.erase(sprite.begin() + handle);
}

// 読み込んだ画像データを全て削除する関数
void GraphicData::AllDeleteGraph()
{
	for (int i = 0, n = tex.size(); i < n; i++)
	{
		tex[i].Delete();	// テクスチャ全削除
	}
	// vector全削除
	tex.clear();	// 要素数を０に
	tex.shrink_to_fit();	// 容量を要素数に合わせる
	sprite.clear();
	sprite.shrink_to_fit();
	graphicmanager.clear();
	graphicmanager.shrink_to_fit();
}

// 画像を描写する関数
void GraphicData::DrawGraph(int x, int y, int handle)
{
	sprite[graphicmanager[handle]].Draw(x, y, direct3d.pDevice3D, tex[graphicmanager[handle]].pTexture);
}